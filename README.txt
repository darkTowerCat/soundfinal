# Digital Chorus Effect

### Install dependencies

You'll need:
* Python 3
* the following libraries:
> sounddevice
> numpy
> argparse
> sys
> time

### Run the program

`python audioShift.py --shift 12 15 17` will run the program and pitchshift the audiostreams by 12, 15, and 17 semitones (an octave, and a 3rd and 5th from there.)

If no arguments are provided, the program defaults to playing the 5th of the incoming audio stream.

`python audioShift.py --delay 50000` will create an echo effect where the current audio pitchshifted stream is averaged with the sound that was observed 50000 samples ago. 