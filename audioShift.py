#websites that provided useful information
#https://github.com/pdx-cs-sound/fm/blob/master/fm.py
#https://bastibe.de/2012-11-02-real-time-signal-processing-in-python.html
#https://stackoverflow.com/questions/8299303/generating-sine-wave-sound-in-python
#https://stackoverflow.com/questions/35344649/reading-input-sound-signal-using-python
#https://github.com/Erxathos/Audio-Pitch-Shift/blob/master/Program.ipynb

from pyaudio import PyAudio, paFloat32, paComplete, paContinue
from time import sleep
from pydub import AudioSegment
import sys
import array
import numpy as np
import pyaudio
import argparse

pa = PyAudio()

FORMAT = pyaudio.paInt16
FRAME_SIZE = 1024
CHUNK = 1024
CHANNELS = 2
RATE = 44100
DELAY_SECONDS = 2
signal = [0] * FRAME_SIZE

volume = 0.7     # range [0.0, 1.0]
fs = 44100       # sampling rate, Hz, must be integer
duration = 1.0   # in seconds, may be float
f = 440.0        # sine frequency, Hz, may be float
n = -10          # pitch shift in semitones
delay_buffer = []


def pitchshift(in_data):
    global shift
    if shift is 0:
        return in_data
    #based on code from https://stackoverflow.com/questions/43963982/python-change-pitch-of-wav-file
    data = np.fromstring(in_data, dtype=np.int16)
    left, right = data[0::2], data[1::2]  # left and right channel
    lf, rf = np.fft.rfft(left), np.fft.rfft(right) #use Fast Fourier Transform from numpy

    #Roll the array to increase the pitch.
    lf, rf = np.roll(lf, shift), np.roll(rf, shift)

    #The highest frequencies roll over to the lowest ones.That's not what we want, so zero them.
    lf[0:shift], rf[0:shift] = 0, 0

    #Now use the inverse Fourier transform to convert the signal back into amplitude.
    nl, nr = np.fft.irfft(lf), np.fft.irfft(rf)

    #Combine the two channels.
    ns = np.column_stack((nl, nr)).ravel().astype(np.int16)

    return ns.tostring()


DELAY_FRAMES = DELAY_SECONDS * RATE


def delay(in_data):
    delay_buffer.append(in_data)
    global delay_size
    delay_size += 1
    if delay_size > DELAY_FRAMES:
        delay_size -= 1
        return delay_buffer.pop(0)
    return in_data


def callback(in_data, frame_count, time_info, status_flags):
    if status_flags:
        print("Playback Error: %i" % status_flags)

    in_data = delay(in_data)
    shifted = pitchshift(in_data)
    return shifted, pyaudio.paContinue

print(sys.argv)
#command line effects
parser = argparse.ArgumentParser(description='Digital Chorus Effect')
parser.add_argument('--shift','-s', dest='shift', default=8, type=int,
                    help='(List of) semitones to shift the input audio.')

parser.add_argument('-d','--delay', dest='delay_size', default=0, type=int,
                    help='Number of samples to delay the pitch shifted copies.')

#set global variables
args = parser.parse_args()
shift = args.shift
delay_size = args.delay_size

stream = pa.open(format=FORMAT,
                 channels=CHANNELS,
                 rate=RATE,
                 input=True,
                 output=True,
                 stream_callback=callback)

print("* recording")

frames = []

stream.start_stream()

while stream.is_active():
    sleep(0.1)

stream.stop_stream()
stream.close()

pa.terminate()
